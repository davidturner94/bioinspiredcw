import random
import numpy as np
from graphviz import Digraph
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
from tqdm import tqdm
from activations import *
import json
from utils import load_data_from_file
import PSO


class ArtificialNeuralNetwork:

    def __init__(self, network_shape: list, file: str, input_data: list, target_outputs: list, activation=activations[1](),
                 learning_rate=0.1, epochs=100, weights=True, optimiser='backprop'):

        # Init variables

        # data
        self.input_data = input_data
        self.output_data = []
        self.target_outputs = target_outputs

        # network_dimensions
        self.input_dim = len(input_data[0])
        self.hidden_shape = network_shape
        self.output_dim = len(target_outputs[0])

        # network variables
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.optimiser = optimiser
        self.activation = activation
        self.layers = []

        # Network error vals
        self.error_over_time = []
        self.epoch_count = 0
        self.ms_error = float('inf')

        self.file = file

        self.network_shape = [self.input_dim]+network_shape+[self.output_dim]
        self.layers = list(range(len(self.network_shape)))
        self.create_network(weights)

    def __str__(self):
        return f'Number of Layers: {len(self.network_shape)} \nNodes per layer: {self.network_shape} \nActivation Function: {self.activation}'

    def create_network(self, weights=True):
        for neurons, n in zip(self.network_shape, range(len(self.network_shape))):
            if weights == True:
                if n == 0:
                    self.layers[n] = Layer(neurons)
                else:
                    self.layers[n] = Layer(
                        neurons, self.network_shape[n-1], self.activation)
            else:
                if n == 0:
                    self.layers[n] = Layer(neurons)
                else:
                    self.layers[n] = Layer(number_of_neurons=neurons, input_dims=self.network_shape[n-1],
                                           layer_bias=weights[n]['layer_bias'], activation=self.activation,
                                           neuron_weights=weights[n]['weights'])

    def plot_data(self, output_data=True, save=False):
        """

        :param output_data:
        :param save:
        :return:
        """
        if len(self.input_data[0]) == 1:
            plt.scatter(x=self.input_data, y=self.target_outputs, c='red')
            if output_data:
                plt.scatter(x=self.input_data, y=[x[0]
                                                  for x in self.output_data], c='Blue')
            if save:
                plt.savefig(
                    f'Data/figs/{self.file.split(".")[0].split("/")[-1]}.png')
            plt.show()
        else:
            fig = plt.figure()
            # axs[0].axes(projection='3d')

            x = np.array([x[0] for x in self.input_data])
            y = np.array([y[1] for y in self.input_data])
            z = np.array([z for z in self.target_outputs])
            z = np.reshape(z, x.shape)

            axs1 = fig.add_subplot(2, 2, 1, projection='3d')
            axs1.set_title('Actual Data')
            axs1.plot_trisurf(x, y, z, cmap='BrBG')

            axs2 = fig.add_subplot(2, 2, 2, projection='3d')
            axs2.set_title('Extimated Data')
            axs2.plot_trisurf(x, y, np.array(
                [z[0] for z in self.output_data]).reshape(x.shape))

            axs3 = fig.add_subplot(2, 2, 3, projection='3d')
            axs3.set_title("Scatter of actual and estimated")
            axs3.scatter(x, y, [z for z in self.target_outputs])
            axs3.scatter(x, y, [z[0] for z in self.output_data])

            if save:
                plt.savefig(
                    f'Data/figs/{self.file.split(".")[0].split("/")[-1]}.png')
            plt.show()

    def network_graph(self):
        g = Digraph('G', filename='network_topology.gv',
                    graph_attr={'splines': 'line'})
        g.attr(rankdir='LR')
        num_layers = len(self.layers)

        node_names = []

        for layer, l in zip(self.layers, range(num_layers)):
            if l == 0:
                with g.subgraph(name='cluster_input') as c:
                    c.attr(style='filled', color='lightgrey')
                    c.node_attr.update(style='filled', color='white')
                    edges = []
                    for neuron, n in zip(layer.neurons, range(len(layer.neurons))):
                        c.node(f'i{n}')
                        edges.append(f'i{n}')
                    node_names.append(edges)

                    c.attr(label='input')

            elif l == num_layers - 1:
                with g.subgraph(name='cluster_output') as c:
                    c.attr(style='filled', color='lightgrey')
                    c.node_attr.update(style='filled', color='white')
                    c.node('o0')
                    node_names.append(['o0'])
                    c.attr(label='output')
            else:

                with g.subgraph(name=f'cluster_{l}') as c:
                    c.attr(style='filled', color='lightgrey')
                    c.node_attr.update(style='filled', color='white')
                    edges = []
                    for neuron, n in zip(layer.neurons, range(len(layer.neurons))):
                        edge_name = f'h{l}{n}'
                        edges.append(edge_name)
                        c.node(edge_name)
                    node_names.append(edges)
                    c.attr(label=f'hidden_layer_{l}')

        for layer in range(len(node_names)-1):
            for node in range(len(node_names[layer])):
                for node_plusone in range(len(node_names[layer+1])):
                    g.edge(node_names[layer][node],
                           node_names[layer+1][node_plusone])

        g.view()

    def forward_pass(self):
        """
        This Method needs to be reworked for efficiency. Need an input array and a weight matrix array.
        Inputs should be verticle array and weights horizontal  then take the dot of the two arrays.
        set these for each neuron in the neuron class to keep track for back prop.
        :return:
        """

        # Cycle through dataset
        self.output_data = []
        for data in range(len(self.input_data)):
            # For each data point forward pass through the network
            for layer, l in zip(self.layers, range(len(self.layers))):
                # if the layer is the input layer then set the input for the network
                if l == 0:
                    for neuron, n in zip(layer.neurons, range(len(layer.neurons))):
                        # Initialise the input layer with the dataset
                        neuron.output = self.input_data[data][n]
                else:
                    # Create output list from previous layer
                    self.layers[l-1].get_outputs()
                    prev_layer_outputs = self.layers[l-1].layer_outputs
                    # create weight matrix for current layer
                    layer.get_weights()
                    curr_layer_weights = layer.weights

                    # print(f'Prev layer outputs {prev_layer_outputs}')
                    # print(f'Curr layer weights {curr_layer_weights}')
                    # print(f'Current layer weights: {curr_layer_weights} \n prev layer output {prev_layer_outputs}')
                    dot = np.dot(curr_layer_weights, prev_layer_outputs)
                    # print(f'dot of curr weights and prev layer {dot}')
                    # print(f'Bias: {layer.layerwise_bias}')
                    # print(f'dot plus bias: {dot + layer.layerwise_bias}')
                    d_b = dot + layer.layerwise_bias
                    for neuron, n in zip(layer.neurons, range(len(d_b))):
                        # print(d_b[n])
                        neuron.value = d_b[n]
                        # print(f'Neuron Set Value: {neuron.value}')
                        neuron.activate()
                        # print(neuron.output)
                    if l == len(self.layers) - 1:
                        self.output_data.append(
                            [x.output for x in layer.neurons] +
                            [self.target_outputs[data]]
                        )
            # This is where back propogation will go.
            if self.optimiser == 'backprop':
                self.backprop(self.target_outputs[data])
                self.update_weights()

        self.mse()

        self.error_over_time.append((self.epoch_count, self.ms_error))
        self.epoch_count += 1

    def plot_error(self):
        # print(self.error_over_time)
        plt.plot(*zip(*self.error_over_time))
        plt.show()

    def mse(self):
        total = 0
        for data in self.output_data:
            total += math.pow((data[1] - data[0]), 2)

        self.ms_error = (1/len(self.output_data))*total

    def backprop(self, expected):

        for i in reversed(range(len(self.layers))):
            layer = self.layers[i]
            errors = []
            if i != len(self.layers) - 1:
                for j in range(len(layer.neurons)):
                    error = 0.0
                    for neuron in self.layers[i + 1].neurons:
                        error += neuron.weights[j] * neuron.delta
                        errors.append(error)
            else:
                for j in range(len(layer.neurons)):
                    neuron = self.layers[i].neurons[j]

                    errors.append(expected - neuron.output)
            for j in range(len(layer.neurons)):
                neuron = layer.neurons[j]
                neuron.delta = errors[j] * neuron.activation_derivative()

    def update_weights(self):
        # weight =  weight + delta * learning rate * input
        for i in range(1, len(self.layers)):
            layer_delta = 0
            inputs = [x.output for x in self.layers[i-1].neurons]
            for neuron in self.layers[i].neurons:
                for w in range(len(inputs)):
                    neuron.weights[w] += self.learning_rate * \
                        neuron.delta * inputs[w]
                    layer_delta += neuron.delta
            # self.layers[i].layerwise_bias += self.learning_rate * \
            #     (layer_delta / len(self.layers[i].neurons))

    def save_model(self, filename):
        network = {
            "structure": self.network_shape,
            "file": self.file,
            "activation": f"{self.activation}",
            "layers": [],

        }

        for layer, l in zip(self.layers, range(len(self.layers))):
            if l == 0:
                label = 'Input'
            elif l == len(self.layers) - 1:
                label = "Output"
            else:
                label = "Hidden"

            network["layers"].append(
                {
                    "layer_id": l,
                    "layer_label": label,
                    "number_of_neurons": len(layer.neurons),
                    "layer_bias": layer.layerwise_bias[0],
                    "weights": [],
                })
            for neuron, n in zip(layer.neurons, range(len(layer.neurons))):
                if neuron.weights is not None:
                    weights = [x for x in neuron.weights]
                else:
                    weights = None
                network["layers"][l]['weights'].append({
                    "neuron_id": f"{n}",
                    "weights": weights
                })
        file = filename
        with open(file, 'w') as f:
            json.dump(network, f)

    def train(self):
        """
        add a param that will alternate between particle swarm and backprop
        :return:
        """

        if self.optimiser == 'backprop':
            progress = tqdm(range(self.epochs))
            for i in progress:
                self.forward_pass()
                progress.set_description(f"MSE: {round(self.ms_error, 8)    }")
                if i % 50 == 0:
                    self.save_model(
                        f'Models/{self.file.split("/")[-1].split(".")[0]}/network_{"-".join([str(x) for x in self.network_shape])}_epoch_{self.epoch_count}.model')
        elif self.optimiser == 'pso':
            # particle swarm optimiser is here and uses the number of epochs for training
            pso = PSO.ParticleSwarmOptimiser(15, self.epochs, self.network_shape, min_val=-1, max_val=1,
                                             n_sub_groups=6, network=self)
            pso.train()
            # change weights to global best and run one final pass to get the results for graphing

        # print(self.error_over_time[-100:])

    @classmethod
    def load_from_model(cls, file, input_data, target_outputs):
        with open(file) as f:
            model = json.load(f)

        structure = model["structure"][1:-1]
        file = model['file']
        for _id, a in activations.items():
            if a().__str__() == model['activation']:
                activation = a()
        weights = model['layers']
        return cls(network_shape=structure, file=file, input_data=input_data, target_outputs=target_outputs, activation=activation, weights=weights)

    def to_vector(self):
        out = []
        for layer in self.layers[1:]:
            for neuron in layer.neurons:
                out.extend(neuron.weights)
        return out


class Layer(object):

    def __init__(self, number_of_neurons, input_dims=None, activation=activations[1](), layer_bias=None, neuron_weights=None):
        self.neurons = list(range(number_of_neurons))
        self.layer_outputs = []
        self.activations = None
        self.weights = None

        if neuron_weights is None:
            self.init_neurons(input_dims, activation)
        else:
            self.init_neurons(
                input_dims=input_dims, activation=activation, neuron_weights=neuron_weights)

        if layer_bias is None:
            self.layerwise_bias = np.random.uniform(0, 0.3, 1)
        else:
            self.layerwise_bias = [layer_bias]

    def get_weights(self):
        layer_weights = []
        for neuron in self.neurons:
            row = neuron.weights
            layer_weights.append(row)
        # layer_weights = np.array(layer_weights)

        # print(f'layer weights: {layer_weights.shape}')
        self.weights = layer_weights

    def get_outputs(self):
        vals = []
        for neuron in self.neurons:
            vals.append(neuron.output)
        # vals = np.array(vals)

        self.layer_outputs = vals

    def init_neurons(self, input_dims, activation, neuron_weights=None):
        for neuron in range(len(self.neurons)):
            if neuron_weights is None:
                if input_dims is None:
                    self.neurons[neuron] = Neuron()
                else:
                    self.neurons[neuron] = Neuron(input_dims, activation)
            else:
                if input_dims is None:
                    self.neurons[neuron] = Neuron()
                else:
                    self.neurons[neuron] = Neuron(
                        input_dims, activation, weights=neuron_weights[neuron]['weights'])


class Neuron(object):

    def __init__(self, input_dims=None, activation=activations[1](), weights=None):
        if weights is None:
            if input_dims is not None:
                self.weights = np.random.uniform(-0.3, 0.3, input_dims)
                # self.weights = self.weights.reshape(1, len(self.weights))
            else:
                self.weights = None
        else:
            if input_dims is not None:
                self.weights = np.array(weights)
            else:
                self.weights = None

        self.output = None
        self.value = None
        self.delta = None
        self.activation = activation

    def get_weight(self):
        return self.weights

    def set_weights(self, weights):
        # self.weights = np.asarray(weights)
        self.weights = weights

    def activate(self):
        self.output = self.activation.activation(self.value)

    def activation_derivative(self):
        return self.activation.derivative(self.output)


if __name__ == "__main__":
    file = 'Data/1in_sine.txt'
    i, o = load_data_from_file(file)
    ann = ANN([6], file=file,
              input_data=i, target_outputs=o, epochs=500, activation=activations[2]())
    ann.train()
    ann.plot_data()
    ann.plot_error()

    # ann.serialise()
    # ann1 = ANN.load_from_model(
    #     'network_1_3_3_3_1.model', input_data=i, target_outputs=o)
    # print(ann1)
    # ann1.learning_rate = 0.1

    # ann1.train()
