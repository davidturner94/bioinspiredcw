import numpy as np
import re


def vector_to_network(vector, Network):
    """vector_to_network

    :param vector: 1D vector of network weights in order
    :param ArtificialNeuralNetwork: Reference to the network that is having its weights updated from the vector
    """
    i = 0
    for layer, p in zip(Network.layers[1:], range(len(Network.layers[1:]))):
        for neuron in layer.neurons:
            n_weights = []
            for _ in range(len(Network.layers[p].neurons)):
                # print(p+1, i, vector[i])
                n_weights.append(vector[i])
                if i < len(vector)-1:
                    i = i + 1

            neuron.set_weights(n_weights)


def create_rand_position(dimensions, min_val, max_val):
    """create_rand_position

    :param dimensions:
    :param min_val:
    :param max_val:
    """
    out_array = []
    for i in range(dimensions):
        out_array.extend([np.random.uniform(min_val, max_val)])

    return np.asarray(out_array)


def load_data_from_file(file):
    """load_data_from_file

    :param file:
    """
    target_outputs = []
    input_data = []
    with open(file, 'r') as f:
        line = f.readline()
        while line:
            data_formatted = [float(x)
                              for x in re.split("\s+", line.strip())]

            target_outputs.append([data_formatted[-1]])
            input_data.append(data_formatted[:-1])
            line = f.readline()

    return input_data, target_outputs


if __name__ == "__main__":
    print(create_rand_position([10], 0, 1))
